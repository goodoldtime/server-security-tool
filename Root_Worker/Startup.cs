﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Root_Worker
{
    class Startup
    {
        private void Start()
        {
            string username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string Domain = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;

            string RootDomain = "lemon.fastwebserver.de";



            if (Domain == RootDomain)
            {

                switch (username)
                {
                    case "L091\\Administrator":
                        Console.WriteLine("Case 1");
                        break;
                    case "L091\\Felix":
                        Console.WriteLine("Case 2");
                        break;
                    case "L091\\Kevin":
                        Console.WriteLine("Case 2");
                        break;
                    case "L091\\Marco":
                        Console.WriteLine("Case 2");
                        break;
                    case "L091\\Ramon":
                        Console.WriteLine("Case 2");
                        break;
                    case "L091\\Rouven":
                        Console.WriteLine("Case 2");
                        break;
                    case "L091\\Sedva":
                        Console.WriteLine("Case 2");
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }
            }
            else
            {
                Login fenster = new Login();
                fenster.Show();
            }
        }

    }
}
