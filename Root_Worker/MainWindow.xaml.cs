﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Root_Worker
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool inNavigation = false;
        private NavigatingCancelEventArgs navArgs = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            //else
            //{
            //    Login
            //    MessageBox.Show("Die Sicherheitsrichtlinien lassen es nicht zu, das Sie diese Anwendung starten und dementsprechend verwenden können. Bei Problemen oder Fragen wenden Sie sich an den Entwickler.", "Error: Security Policies", MessageBoxButton.OK, MessageBoxImage.Error);
            //    //Application.Current.Shutdown();
            //}
        }

        #region Animation_and_Navigation

        private void NavigationFrame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (!inNavigation)
            {
                e.Cancel = true;
                navArgs = e;

                DoubleAnimation animation = new DoubleAnimation();
                animation.From = 1f;
                animation.To = 0f;
                animation.Duration = new Duration(TimeSpan.FromMilliseconds(200));
                animation.Completed += Animation_Completed;
                NavigationFrame.BeginAnimation(OpacityProperty, animation);

                inNavigation = true;
            }
        }

        private void Animation_Completed(object sender, EventArgs e)
        {
            switch (navArgs.NavigationMode)
            {
                case NavigationMode.New:
                    if (navArgs.Uri == null)
                        NavigationFrame.Navigate(navArgs.Content);
                    else
                        NavigationFrame.Navigate(navArgs.Uri);
                    break;
                case NavigationMode.Back:
                    NavigationFrame.GoBack();
                    break;
                case NavigationMode.Forward:
                    NavigationFrame.GoForward();
                    break;
                case NavigationMode.Refresh:
                    NavigationFrame.Refresh();
                    break;
            }
            inNavigation = false;

            DoubleAnimation animation = new DoubleAnimation();
            animation.From = 0f;
            animation.To = 1f;
            animation.Duration = new Duration(TimeSpan.FromMilliseconds(450));
            NavigationFrame.BeginAnimation(OpacityProperty, animation);
        }

        #endregion

        private void PerformanceMonitor()
        {

            
        }
    }
}
